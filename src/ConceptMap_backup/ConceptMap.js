import React from 'react';
import TinCan from 'tincanjs';
import ReactDOM from 'react-dom';
import './styles.css';
import Background from './Background';
// import domtoimage from 'dom-to-image';
import Clickables from './Clickables';

var Wdth, mobile = false,
    iphone = false;
window.onresize = function() {
    resizeFunction();
};
window.onload = function() {
    console.log("Onload")
    resizeFunction();
};

const contentHolderCss = {
    backgroundSize: '100% 100%',
    width: '100%',
    height: '100%',
    position: 'relative',
    WebkitTouchCallout: 'none',
    WebkitUserSelect: 'none',
    KhtmlUserSelect: 'none',
    MozUserSelect: 'none',
    MsUserSelect: 'none',
    userSelect: 'none',
}

function resizeFunction() {
    console.log("Entered resize:"+mobile);
    let ContentHolder = document.getElementById('canvasWrapper');
    if (!mobile) {
        if (((window.innerWidth * 9) / 16) <= window.innerHeight) {
            if(ContentHolder) {
                ContentHolder.style.width = window.innerWidth + 'px';
                ContentHolder.style.height = ((window.innerWidth * 9) / 16).toFixed(2) + 'px';
                console.log(ContentHolder.style.width+" ... "+ContentHolder.style.height);
                ContentHolder.style.marginTop = ((window.innerHeight - (window.innerWidth * .5625)) / 2).toFixed(2) + 'px';
                ContentHolder.style.marginLeft = 0;
            }
            Wdth = window.innerWidth;
            //Ht = ((window.innerWidth * 9) / 16);
        } else {
            if(ContentHolder) {
                ContentHolder.style.width = ((window.innerHeight * 16) / 9).toFixed(2) + 'px';
                ContentHolder.style.height = window.innerHeight + 'px';
                console.log(ContentHolder.style.width+" ... "+ContentHolder.style.height);

                ContentHolder.style.marginTop = 0;
                ContentHolder.style.marginLeft = ((window.innerWidth - ((window.innerHeight * 16) / 9)) / 2).toFixed(2) + 'px';
            }
            Wdth = (window.innerHeight * 16) / 9;
            //Ht = (window.innerHeight);
        }
    } else {
        if (document.mozCancelFullScreen || iphone === true) {
            if (window.innerWidth < window.innerHeight || iphone === true) {
                if (iphone === false)
                    document.mozCancelFullScreen();
                //document.getElemen
                //$('.fullScreen').css('display', 'none');
            } else {
                //$('.fullScreen').css('display', 'block');
            }
        }
            if(ContentHolder) {
                ContentHolder.style.width = window.innerWidth + 'px';
                ContentHolder.style.height = ((window.innerWidth * 9) / 16) + 'px';
                ContentHolder.style.marginTop = 0;
                ContentHolder.style.marginLeft = 0;
            }
        Wdth = window.innerWidth;
        //Ht = ((window.innerWidth * 9) / 16);
    }

    if(ContentHolder) {
    setFontSizes1(Wdth);}


    if(typeof(window.setFontSizes)!=="undefined")
        window.setFontSizes(Wdth);
    if(typeof(window.resizeInAnimationElements)!=="undefined")
        window.resizeInAnimationElements(Wdth);

    //debugger;
}

/*function is_function(func) {
    return typeof window[func] !== 'undefined' ;
}*/


function setFontSizes1(Wdth) {
    document.getElementsByClassName('heading')[0].style.fontSize = (Wdth / 1280) * 16+"px";
    document.getElementsByClassName('description')[0].style.fontSize = (Wdth / 1280) * 16+"px";
    document.getElementsByClassName('topic-description')[0].style.fontSize = (Wdth / 1280) * 18+"px";
}


// var unmountBtn = document.getElementById('unmount');
// unmountBtn.addEventListener('click', unmount);
//
// function unmount() {
//     console.log('Unmounting');
//     // ReactDOM.unmountComponentAtNode(document.getElementById('root'));
//     unmountBtn.remove();
// }

function newCheck() {
    // alert('Hello');
    console.log("Hello");
    // document.getElementById("myDialog").showModal();
    // document.getElementById('myModal').style.display = "block";
}

function  saveLrsStatement(startTime, endTime) {
    console.log('Start:'+startTime+'  end:'+ endTime);
    var lrs;

    try {
        lrs = new TinCan.LRS(
            {
                endpoint: "http://localhost:8012/data/xAPI/",
                username: "fcd0d7f803b008ffc9ee94c9d883458642adc1dd",
                password: "ae70814d8d096a9a6b4d50f668ebd815f0a680d8",
                allowFail: false
            }
        );
        console.log('Entered');
    }
    catch (ex) {
        console.log("Failed to setup LRS object: " + ex);
        // TODO: do something with error, can't communicate with LRS
    }

    var statement = new TinCan.Statement(
        {
            "actor": {
                "mbox": "mailto:student_id@fpix.com",
                "objectType": "Agent",
                "name":"FX-U213"
            },
            "verb": {
                "id": "http://adlnet.gov/expapi/verbs/experienced",
                "display": {
                    "und": "experienced"
                }
            },
            "authority": {
                "account": {
                    "homePage": "http://fortunapix.com/",
                    "name": "Fortunapix"
                },
                "objectType": "Agent"
            },

            "object": {
                "id": "8f87ccde-bb56-4c2e-ab83-44982ef22df0",
                "objectType": "StatementRef"
            },
            "result": {
                "response": JSON.stringify({
                    "startTime": startTime,
                    "endTime": endTime
                })
            }
        }




    );

    console.log('LRS start');
    lrs.saveStatement(
        statement,
        {
            callback: function (err, xhr) {
                console.log('Saving....');
                if (err !== null) {
                    if (xhr !== null) {
                        console.log("Failed to save statement: " + xhr.responseText + " (" + xhr.status + ")");
                        // TODO: do something with error, didn't save statement
                        return false;
                    }

                    console.log("Failed to save statement: " + err);
                    // TODO: do something with error, didn't save statement
                    return false;
                }

                console.log("Statement saved");
                // TOOO: do something with success (possibly ignore)
                return true;
            }
        }
    );
}

var startTime, endTime;
export default class ConceptMap extends React.Component {

    constructor() {
        console.log('Constructor')
        super();
        this.state = {
            mobile: false,
            iPhone: false,
            Wdth: 0,
            Ht: 0
        }
    }

    componentWillMount() {
        // console.log('ComponentWillMount');
        /*window.addEventListener("beforeunload", (ev) =>
        {
            endTime = new Date();
            var status = saveLrsStatement(startTime, endTime);
            console.log(status);
            ev.preventDefault();
            ev.returnValue = 'Are you sure you want to close?';
            console.log(ev.returnValue);
            return ev.returnValue

        });
        startTime = new Date();*/
    }

    componentDidMount() {
        console.log('componentDidMount');
        // endTime = new Date();
        // saveLrsStatement(startTime, endTime);
        // saveScreenshot();
    }

    // update(){
    //     console.log('Entered update')
    // }

    /*saveScreenshot() {
        var node = document.getElementById('canvasWrapper');

        // console.log('Root Node...:'+JSON.stringify(node));
        domtoimage.toPng(node)
            .then(function (dataUrl) {
                // console.log('Entered...screenshot:'+dataUrl);
                var img = new Image();
                img.src = dataUrl;
                console.log(img);
                document.body.appendChild(img);
            })
            .catch(function (error) {
                console.error('oops, something went wrong! \n', error);
            });
    }*/

    clickContentInfo(e){
        console.log("Success concept-map"+JSON.stringify(e));
    }

    render() {
        console.log('Render');
        return (<div id={"canvasWrapper"} className={"canvas-class"} >
            < div className={ "contentHolder" }
            style={{...contentHolderCss } } >
            < Background clikedClipNew={this.clickContentInfo}/>

            </div>
            </div>
        )

    }
}
