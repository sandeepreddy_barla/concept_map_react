import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import $ from 'jquery';
/*import config from './config2.json';*/
import './styles.css';
import './ConceptMap'



export default class Clickables extends React.Component{

	constructor(){
		super();
		this.state={
			HeadingArray:[],
			descriptionArray:[],
			previousId:null,
			topicName:''
		}

		this.clikedClip = this.clikedClip.bind(this);
		// this.propmpFunction = this.propmpFunction.bind(this);
		this.getTopicData = this.getTopicData.bind(this);

		
	}

	componentWillMount() {
        // this.propmpFunction('Please enter Topic ID:');
        this.getTopicData();
    }

	/*propmpFunction(msg){
		var topicID = prompt(msg, "");
		if (topicID === null || topicID === "") {
			//this.propmpFunction('Please Enter the correct Topic ID:')
		} else {
			this.getTopicData(topicID)
		}
	}*/

	getTopicData(topicID){
		console.log('get Topic Data'+localStorage.getItem('topicId'));

		topicID = 'b121152';

		var dummyContent = {
			"id":87,
			"topic_id":null,
			"fpix_topic_id":"b121152",
			"folder_name":"b121152_vascular_bundles",
			"description":"Vascular bundle is a part of the transport system in plants that conducts water and other metabolic products.",
			"heading":"\"Dicotyledonous plants\",\"Sieve tubes\",\"Companion cells\",\"Phloem\",\"Phloem parenchyma\",\"Tracheids\",\"Xylem\",\"Xylem parenchyma\",\"Monocotyledonous plants\",\"Phloem\",\"Xylem\"",
			"head_description":"\"A flowering plant consisting of two embryonic seed leaves.\",\"The series of end to end joined cells, through which conduction of food materials takes place.\",\"A special parenchyma cell closely associated with the function of sieve tubes.\",\"Plant tissue that transports sugars and other metabolic products from leaves to other parts of plants.\",\"A living cell found in between sieve tubes and its main function is to store food.\",\"The water conducting cells which transport water from roots to other parts of plant.\",\"The structural part of plant that conducts water from roots to stem.\",\"The living component in xylem tissue which is present in between vessels and xylem fibers.\",\"A flowering plant consisting of one seed leaf.\",\"Phloem is the vascular tissue respnsible for the tarnsport of sugars and mRNA throughout the plant.\",\"Xylem is a vascular tissue in plants which transports water and soluble mineral nutrients from the roots throughout the plant.\"",
			"custom_js":"",
			"animation_js":"function resizeInAnimationElements(Wdth) {\r\n\r\nscale=Wdth/1280;\r\n\r\n}",
			"animation_css":".contentHolder #clip1{width: 10.1%;height: 4.2%;position:absolute;top: 22.7%;left: 40.6%;cursor:pointer;opacity: 0;z-index: 10;border: 1px solid;}.contentHolder #clip2{width: 5.9%;height: 3.5%;position:absolute;top: 11.7%;left: 73.9%;cursor:pointer;opacity: 0;z-index: 10;border: 1px solid;}.contentHolder #clip3{width: 8.5%;height: 3.9%;position:absolute;top: 18.1%;left: 71.4%;cursor:pointer;opacity: 0;z-index: 10;border: 1px solid;}.contentHolder #clip4{width: 3.9%;height: 3.9%;position:absolute;top: 28%;left: 72.8%;cursor:pointer;opacity: 0;z-index: 10;border: 1px solid;}.contentHolder #clip5{width: 10.7%;height: 3.5%;position:absolute;top: 34.9%;left: 69.2%;cursor:pointer;opacity: 0;z-index: 10;border: 1px solid;}.contentHolder #clip6{width: 4.7%;height: 3.4%;position:absolute;top: 57.1%;left: 75.1%;cursor:pointer;opacity: 0;z-index: 10;border: 1px solid;}.contentHolder #clip7{width: 3.1%;height: 3.8%;position:absolute;top: 73.8%;left: 73.5%;cursor:pointer;opacity: 0;z-index: 10;border: 1px solid;}.contentHolder #clip8{width: 9.9%;height: 3.9%;position:absolute;top: 79.9%;left: 70%;cursor:pointer;opacity: 0;z-index: 10;border: 1px solid;}.contentHolder #clip9{width: 12.1%;height: 4.3%;position:absolute;top: 73.7%;left: 40.6%;cursor:pointer;opacity: 0;z-index: 10;border: 1px solid;}.contentHolder #clip10{width: 4.2%;height: 4%;position:absolute;top: 23.4%;left: 55.8%;cursor:pointer;opacity: 0;z-index: 10;border: 1px solid;}.contentHolder #clip11{width: 3.4%;height: 3.6%;position:absolute;top: 73.5%;left: 56.5%;cursor:pointer;opacity: 0;z-index: 10;border: 1px solid;}.contentHolder #clipdummy1{opacity: 0.4;width: 10.1%;height: 4.2%;position:absolute;top: 22.8%;left: 40.6%;cursor:pointer;background:#FFFF00;display: none;z-index: 2;}.contentHolder #clipdummy2{opacity: 0.4;width: 6%;height: 1.8%;position:absolute;top: 13.5%;left: 73.9%;cursor:pointer;background:#FFFF00;display: none;z-index: 2;}.contentHolder #clipdummy3{opacity: 0.4;width: 8.6%;height: 2%;position:absolute;top: 20.1%;left: 71.4%;cursor:pointer;background:#FFFF00;display: none;z-index: 2;}.contentHolder #clipdummy4{opacity: 0.4;width: 4%;height: 2%;position:absolute;top: 30%;left: 72.8%;cursor:pointer;background:#FFFF00;display: none;z-index: 2;}.contentHolder #clipdummy5{opacity: 0.4;width: 10.8%;height: 2%;position:absolute;top: 36.5%;left: 69.2%;cursor:pointer;background:#FFFF00;display: none;z-index: 2;}.contentHolder #clipdummy6{opacity: 0.4;width: 4.8%;height: 2%;position:absolute;top: 58.6%;left: 75.1%;cursor:pointer;background:#FFFF00;display: none;z-index: 2;}.contentHolder #clipdummy7{opacity: 0.4;width: 3.2%;height: 2.1%;position:absolute;top: 75.5%;left: 73.5%;cursor:pointer;background:#FFFF00;display: none;z-index: 2;}.contentHolder #clipdummy8{opacity: 0.4;width: 10%;height: 2%;position:absolute;top: 81.8%;left: 70%;cursor:pointer;background:#FFFF00;display: none;z-index: 2;}.contentHolder #clipdummy9{opacity: 0.4;width: 12.2%;height: 4.2%;position:absolute;top: 73.8%;left: 40.6%;cursor:pointer;background:#FFFF00;display: none;z-index: 2;}.contentHolder #clipdummy10{opacity: 0.4;width: 4.2%;height: 2%;position:absolute;top: 23.6%;left: 55.8%;cursor:pointer;background:#FFFF00;display: none;z-index: 2;}.contentHolder #clipdummy11{opacity: 0.4;width: 3.5%;height: 2%;position:absolute;top: 73.6%;left: 56.5%;cursor:pointer;background:#FFFF00;display: none;z-index: 2;}.contentHolder .explanationDiv{display: none;width: 15%;height: 11%;position:absolute;top: 10%;left: 41.5%;background:#FFFFC6;border:2px solid #CFCFCF;padding: 0.1%;overflow: auto;z-index: 9999;}",
			"created_at":"2017-05-08T05:28:31.000Z",
			"updated_at":"2017-05-08T05:28:31.000Z",
			"url":"http://content.fortunapix.com/conceptmaps/87.json"
		};

		$.getJSON("config2.json", function(json) {
			// console.log("JSON Info Success:"+JSON.stringify(json)); // this will show the info it in firebug console

			var remoteData = dummyContent;
			remoteData["heading"] = JSON.parse("[" + dummyContent.heading + "]");
			remoteData["head_description"] = JSON.parse("[" + dummyContent.head_description + "]");

			var extraElements = document.getElementById('contentHolder');
			var Wdth = document.getElementById('contentHolder').clientWidth;
			//debugger;
			var relativeImagePath = remoteData["animation_css"];
			var regex1 = new RegExp('../animations/', 'g');
			var regex2= new RegExp('../images/', 'g');


			var hostUrl = window.location.protocol+'//'+window.location.hostname;

			if(window.location.port){
				hostUrl = window.location.protocol+'//'+window.location.hostname+':'+window.location.port;
			}

			var assetsURL = hostUrl+'/assets/';


			var relPathAnims = assetsURL+topicID+'/conceptmap/animations/';
			var relPathImages = assetsURL+topicID+'/conceptmap/images/';

			relativeImagePath = relativeImagePath.replace(regex1,relPathAnims);
			relativeImagePath = relativeImagePath.replace(regex2,relPathImages);

			//debugger;

			//var cssElem = document.createElement('style');
			$('.topic-description').html(remoteData['description']);
			// console.log(remoteData['description'])
			var cssElem = document.createElement('style');

			// var relativeImagePath = ".contentHolder #clip1{width: 6.2%;height: 4%;position:absolute;top: 75.9%;left: 13%;cursor:pointer;opacity: 0;z-index: 10;}.contentHolder #clip2{width: 4%;height: 4.2%;position:absolute;top: 19.2%;left: 30.4%;cursor:pointer; opacity: 0;z-index: 10;}.contentHolder #clip3{width: 7.8%;height: 4.2%;position:absolute;top: 48%;left: 27.8%;cursor:pointer;opacity: 0;z-index: 10;}.contentHolder #clip4{width: 7.5%;height: 4.2%;position:absolute;top: 81.2%;left: 30%;cursor:pointer; opacity: 0;z-index: 10;}.contentHolder #clip5{width: 6%;height: 4%;position:absolute;top: 48.3%;left: 47.7%;cursor:pointer; opacity: 0;z-index: 10;}.contentHolder #clip6{width: 6.5%;height: 3.6%;position:absolute;top: 42.8%;left: 55.8%;cursor:pointer;opacity: 0;z-index: 10;}.contentHolder #clip7{width: 7.1%;height: 4.2%;position:absolute;top: 72.6%;left: 56%;cursor:pointer; opacity: 0;z-index: 10;}.contentHolder #clip8{width: 5.8%;height: 3.7%;position:absolute;top: 22.8%;left: 64.9%;cursor:pointer;opacity: 0;z-index: 10;}.contentHolder #clip9{width: 6%;height: 3.9%;position:absolute;top: 37%;left: 64.9%;cursor:pointer; opacity: 0;z-index: 10;}.contentHolder #clip10{width: 5%;height: 3.8%;position:absolute;top: 51.6%;left: 64.9%;cursor:pointer; opacity: 0;z-index: 10;}.contentHolder #clip11{width: 6.5%;height: 4%;position:absolute;top: 66.6%;left: 64.9%;cursor:pointer;opacity: 0;z-index: 10;}.contentHolder #clip12{width: 5.7%;height: 3.95%;position:absolute;top: 78.8%;left: 64.9%;cursor:pointer; opacity: 0;z-index: 10;}.contentHolder #clip13{width: 3.3%;height: 4%;position:absolute;top: 63.7%;left: 76.5%;cursor:pointer;opacity: 0;z-index: 10;}.contentHolder #clip14{width: 5%;height: 2%;position:absolute;top: 74.2%;left: 75.1%;cursor:pointer; opacity: 0;z-index: 10;}.contentHolder #clipdummy1{opacity: 0.4; width: 6%;height: 2.2%;position:absolute;top: 77.8%;left: 13.2%;cursor:pointer;background:#FFFF00;display: none;z-index: 2;}.contentHolder #clipdummy2{opacity: 0.4;width: 4.2%;height: 2.3%;position:absolute;top: 21.4%;left: 30.4%;cursor:pointer;background:#FFFF00;display: none;z-index: 2;}.contentHolder #clipdummy3{opacity: 0.4;width: 7.8%;height: 2.2%;position:absolute;top: 50.3%;left: 27.9%;cursor:pointer;background:#FFFF00;display: none;z-index: 2;}.contentHolder #clipdummy4{opacity: 0.4;width: 7.1%;height: 2.2%;position:absolute;top: 81.3%;left: 30.4%;cursor:pointer; background:#FFFF00;display: none;z-index: 2;}.contentHolder #clipdummy5{opacity: 0.4;width: 6.2%;height: 2.2%;position:absolute;top: 50.3%;left: 47.8%;cursor:pointer; background:#FFFF00;display: none;z-index: 2;}.contentHolder #clipdummy6{opacity: 0.4;width: 6.7%;height: 2.2%;position:absolute;top: 42.5%;left: 55.8%;cursor:pointer;background:#FFFF00;display: none;z-index: 2;}.contentHolder #clipdummy7{opacity: 0.4;width: 7.4%;height: 2.1%;position:absolute;top: 74.8%;left: 55.9%;cursor:pointer;background:#FFFF00;display: none;z-index: 2;}.contentHolder #clipdummy8{opacity: 0.4;width: 5.8%;height: 2.2%;position:absolute;top: 24.5%;left: 65%;cursor:pointer;background:#FFFF00;display: none;z-index: 2;}.contentHolder #clipdummy9{opacity: 0.4;width: 6.1%;height: 2.2%;position:absolute;top: 38.9%;left: 64.9%;cursor:pointer;background:#FFFF00;display: none;z-index: 2;}.contentHolder #clipdummy10{opacity: 0.4;width: 5.2%;height: 2.2%;position:absolute;top: 53.5%;left: 64.9%;cursor:pointer;background:#FFFF00;display: none;z-index: 2;}.contentHolder #clipdummy11{opacity: 0.4;width: 6.7%;height: 2.2%;position:absolute;top: 68.5%;left: 64.9%;cursor:pointer;background:#FFFF00;display: none;z-index: 2;}.contentHolder #clipdummy12{opacity: 0.4;width: 5.8%;height: 2.2%;position:absolute;top: 80.9%;left: 65%;cursor:pointer;background:#FFFF00;display: none;z-index: 2;}.contentHolder #clipdummy13{opacity: 0.4;width: 3.6%;height: 2%;position:absolute;top: 65.8%;left: 76.5%;cursor:pointer;background:#FFFF00;display: none;z-index: 2;}.contentHolder #clipdummy14{opacity: 0.4;width: 3.6%;height: 2%;position:absolute;top: 74.2%;left: 76.5%;cursor:pointer; background:#FFFF00;display: none;z-index: 2;}.contentHolder .explanationDiv{display: none;width: 15%;height: 13%;position:absolute;top: 5%;left: 34.5%;background: #FFFFC6;border: 2px solid #CFCFCF;padding: 0.1%;overflow: auto;z-index: 9999;}.animContainer0{   position: absolute;top: 40.5%;left: 38%;z-index:999;display:block;}.animDiv0 {background-image: url('../animations/anim1.png');width: 194px;height: 218px;top: 0;left: 0;position: absolute;    -webkit-animation: play 8s steps(40, end) infinite;       -moz-animation: play 8s steps(40, end) infinite;        -ms-animation: play 8s steps(40, end) infinite;         -o-animation: play 8s steps(40, end) infinite;            animation: play 8s steps(40, end) infinite;} @-webkit-keyframes play {   from { background-position:    0px; }     to { background-position: -7760px; }}@-moz-keyframes play {   from { background-position:    0px; }     to { background-position: -7760px; }}@-ms-keyframes play {   from { background-position:    0px; }     to { background-position: -7760px; }}@-o-keyframes play {   from { background-position:    0px; }     to { background-position: -7760px; }}@keyframes play {   from { background-position:    0px; }     to { background-position: -7760px; }}"
			cssElem.innerHTML = relativeImagePath + ' .contentbg{ background: url('+assetsURL+topicID+'/conceptmap/images/contentbg.png) no-repeat center;background-size: 100% 100%;}';
			ReactDOM.findDOMNode(extraElements).append(cssElem);


			var jsElem = document.createElement('script');

			jsElem.innerHTML = remoteData["animation_js"]+";\nif(typeof(setFontSizes)!=='undefined'){setFontSizes($('.contentHolder').width())}\nif(typeof(resizeInAnimationElements)!=='undefined'){resizeInAnimationElements($('.contentHolder').width())}";
			//jsElem.innerHTML = "window.onresize = function() {console.log($('.contentHolder').width())};";

			var div = document.createElement('div');
			div.innerHTML = remoteData.custom_js;
			ReactDOM.findDOMNode(extraElements).append(div);
			document.getElementsByTagName('head')[0].appendChild(jsElem);

			this.setState({
				HeadingArray:remoteData.heading,
				descriptionArray:remoteData.head_description,
				topicName:remoteData.folder_name
			});

		}.bind(this))
		.catch(error=> {
			console.log('Entered error');
		 // this.propmpFunction("Please enter the valid Topic ID");
		 });
	}


	clikedClip(e){
		console.log('Clicked:'+JSON.stringify(e.target.dataset));
		var currentElement = parseInt(e.target.dataset.id);
		if(currentElement!==this.state.previousId){

			ReactDOM.findDOMNode(this.refs.explanationDiv).style.display="block";
			document.getElementById('clipdummy'+currentElement).style.display="block";

			ReactDOM.findDOMNode(this.refs.heading).innerHTML=this.state.HeadingArray[currentElement-1];
			ReactDOM.findDOMNode(this.refs.description).innerHTML=this.state.descriptionArray[currentElement-1];
			if(this.state.previousId!==null){
				document.getElementById('clipdummy'+this.state.previousId).style.display="none";
			}
			this.setState({previousId: currentElement});
		}else{
			ReactDOM.findDOMNode(this.refs.explanationDiv).style.display="none";
			document.getElementById('clipdummy'+currentElement).style.display="none";
			this.setState({previousId: null});
		}

		this.props.cliked(e.target.dataset);
	}


	render(){
        console.log("clickables")

		this.clickables = this.state.HeadingArray.map((clickable,i)=>
			<div key={i}>
				<div className={"clip"+(i+1)}  id={"clip"+(i+1)} data-id={(i+1)} onClick={this.clikedClip}></div>
				<div className={"clipdummy"+(i+1)} id={"clipdummy"+(i+1)} ref="dummy"></div>
			</div>	
		)

		return(
			<div>
				<div className="topic-description"></div>
				<div>{this.clickables}</div>
				<div className={"explanationDiv"} ref="explanationDiv">
					<div className={"heading"} ref="heading" ></div>
					<div className={"description"} ref="description" ></div>
				</div>
			</div>
		)
	}


}
				