import React from 'react';
import './styles.css';
import config from '../config2.json';





export default class TopicTtitles extends React.Component{


	constructor(props){
		super(props);
		this.state={
			topicName:config.topicName,
			supType:config.supType,
			instText:config.instText
		}
		this.propmpFunction = this.propmpFunction.bind(this);
		this.getTopicData = this.getTopicData.bind(this);
	}
	componentWillMount() {
		this.propmpFunction('Please enter Topic ID:');
	}
   	propmpFunction(msg){
   		var topicID = prompt(msg, "b121002");
	    if (topicID === null || topicID === "") {
	    	//this.propmpFunction('Please Enter the correct Topic ID:')
	    } else {
	        this.getTopicData(topicID)
	    }
   	}

   	getTopicData(topicID){
   		axios.post('http://localhost/react/conceptMapDetails.php',{topicID: topicID})
		  	.then(data=>{
		        if(data!==""){
					this.setState({HeadingArray:data.data.cfdata})
				}
		  	})
		  	.catch(error=> {
		    this.propmpFunction("Please enter the valid Topic ID");
		});
   	}




	render(){
		return(
			<div>
				<div className={"topicName"}>{this.state.topicName}</div>
				<div className={"supType"}>{this.state.supType}</div>
				<div className={"instText"}>{this.state.instText}</div>
			</div>
		)
	}
}