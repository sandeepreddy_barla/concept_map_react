import React from 'react';
import ReactDOM from 'react-dom';
import ConceptMap from './ConceptMap';
//import Keywords from './Keywords/Keywords';

ReactDOM.render(
  <ConceptMap/>,
  document.getElementById('root')
);