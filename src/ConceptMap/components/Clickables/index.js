import React from 'react';
import ReactDOM from 'react-dom';
// import $ from 'jquery';
import '../../styles.css';
// import '../../ConceptMap'



export default class Clickables extends React.Component{

    constructor(props){
        super(props);
        this.state= this.props.data;

        this.clikedClip = this.clikedClip.bind(this);

    }

    componentWillReceiveProps(nextProps) {
        // console.log("Enteres"+JSON.stringify(nextProps));
        this.setState(nextProps.data);
    }

    clikedClip(e){
        console.log('Clicked:'+JSON.stringify(e.target.dataset));
        var currentElement = parseInt(e.target.dataset.id);
        if(currentElement!==this.state.previousId){

            ReactDOM.findDOMNode(this.refs.explanationDiv).style.display="block";
            document.getElementById('clipdummy'+currentElement).style.display="block";

            ReactDOM.findDOMNode(this.refs.heading).innerHTML=this.state.HeadingArray[currentElement-1];
            ReactDOM.findDOMNode(this.refs.description).innerHTML=this.state.descriptionArray[currentElement-1];
            if(this.state.previousId!==null){
                document.getElementById('clipdummy'+this.state.previousId).style.display="none";
            }
            this.setState({previousId: currentElement});
        }else{
            ReactDOM.findDOMNode(this.refs.explanationDiv).style.display="none";
            document.getElementById('clipdummy'+currentElement).style.display="none";
            this.setState({previousId: null});
        }

        this.props.cliked(e.target.dataset);
    }


    render(){
        console.log("clickables");

        this.clickables = this.state.HeadingArray.map((clickable, i) =>
            <div key={i}>
                <div className={"clip" + (i + 1)} id={"clip" + (i + 1)} data-id={(i + 1)}
                     onClick={this.clikedClip}></div>
                <div className={"clipdummy" + (i + 1)} id={"clipdummy" + (i + 1)} ref="dummy"></div>
            </div>
        );

        return(
            <div>
                <div>{this.clickables}</div>
                <div className={"explanationDiv"} ref="explanationDiv">
                    <div className={"heading"} ref="heading" ></div>
                    <div className={"description"} ref="description" ></div>
                </div>
            </div>
        )
    }
}
